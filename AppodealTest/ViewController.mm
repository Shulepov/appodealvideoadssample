//
//  ViewController.m
//  AppodealTest
//
//  Created by Mikhail Shulepov on 11.12.15.
//  Copyright © 2015 Planemo. All rights reserved.
//

#import "ViewController.h"
#import "AppodealCpp/Appodeal.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showAds {
    Advertise::AppodealAds::getInstance()->presentVideoInterstitial([]() {
        
    });
}

@end
