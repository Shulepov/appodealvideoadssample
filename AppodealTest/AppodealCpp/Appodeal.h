//
//  AppodealAds.h
//  CCLibs
//
//  Created by Mikhail Shulepov on 04/07/15.
//
//

#ifndef CCLibs_AppodealAds_h
#define CCLibs_AppodealAds_h

#include <string>
#include <functional>
#include <memory>

namespace Advertise {
    
    class AppodealAds {
    public:
        static std::shared_ptr<AppodealAds> getInstance();
        virtual ~AppodealAds() {}
        
        virtual void startWithAppKey(const std::string &key) = 0;
        
        virtual void removeBanner() = 0;
        virtual void presentBanner(float xPercent, float yPercent, float yAnchor) = 0;
        
        virtual void presentVideoInterstitial(std::function<void()> completion) = 0;
        virtual void presentInterstitial(std::function<void()> completion) = 0;
        virtual void presentInterstitialStaticOrVideo(std::function<void()> completion) = 0;
        virtual void presentRewardedVideo(std::function<void(bool success)> completion) = 0;
        
        virtual bool hasCachedRewardedVideo() const = 0;
        
    protected:
        AppodealAds() {}
    };
    

}



#endif
