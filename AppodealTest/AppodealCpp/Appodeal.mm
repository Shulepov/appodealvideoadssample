//
//  Appodeal.cpp
//  CCLibs
//
//  Created by Mikhail Shulepov on 04/07/15.
//
//

#import <Appodeal/Appodeal.h>

#include "Appodeal.h"

NSString *NSStringFromStdString(const std::string &str) {
    return [NSString stringWithUTF8String:str.c_str()];
}

@interface IOSAppodealDelegate : NSObject<AppodealInterstitialDelegate, AppodealVideoDelegate, AppodealRewardedVideoDelegate>

@property (nonatomic) std::function<void()> interstitialCompletion;
@property (nonatomic) std::function<void(bool success)> rewardedVideoCompletion;

@end

@implementation IOSAppodealDelegate {
    BOOL rewardedVideoPresented;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        rewardedVideoPresented = NO;
    }
    return self;
}

#pragma mark Interstitial

- (void)interstitialDidLoadAd {
    NSLog(@"Appodeal: Interstitial did load");
}

- (void)interstitialDidFailToLoadAd {
    NSLog(@"Appodeal: Interstitial did fail to load");
    if (_interstitialCompletion) {
        _interstitialCompletion();
        _interstitialCompletion = nullptr;
    }
}

- (void)interstitialDidDismiss {
    if (_interstitialCompletion) {
        _interstitialCompletion();
        _interstitialCompletion = nullptr;
    }
}

#pragma mark Video

- (void)videoDidLoadAd {
    NSLog(@"Appodeal: video did load");
}

- (void)videoDidFailToLoadAd {
    NSLog(@"Appodeal: video did fail to load");
    if (_interstitialCompletion) {
        _interstitialCompletion();
        _interstitialCompletion = nullptr;
    }
}

- (void)videoWillDismiss {
    if (_interstitialCompletion) {
        _interstitialCompletion();
        _interstitialCompletion = nullptr;
    }
}

- (void)videoDidFinish {

}

#pragma mark - Rewarded videos

- (void)rewardedVideoDidLoadAd {
    //rewarded video ready
}

- (void)rewardedVideoDidFailToLoadAd {
    NSLog(@"Appodeal: rewarded video did fail to load");
    /*if (_rewardedVideoCompletion && !rewardedVideoPresented) {
        _rewardedVideoCompletion(false);
        _rewardedVideoCompletion = nullptr;
    }*/
}

- (void)rewardedVideoDidPresent {
    rewardedVideoPresented = YES;
}

- (void)rewardedVideoWillDismiss {
    NSLog(@"Rewarded video will dismiss");
    rewardedVideoPresented = NO;
    if (_rewardedVideoCompletion) {
        _rewardedVideoCompletion(true);
        _rewardedVideoCompletion = nullptr;
    }
}

- (void)rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(NSString *)rewardName {
    NSLog(@"Rewarded video did finish");
    rewardedVideoPresented = NO;
    if (_rewardedVideoCompletion) {
        _rewardedVideoCompletion(true);
        _rewardedVideoCompletion = nullptr;
    }
}

@end

class IOSAppodeal: public Advertise::AppodealAds {
private:
    IOSAppodealDelegate *iosDelegate;
    
public:
    virtual void startWithAppKey(const std::string &key) override {
        //[Appodeal disableNetworkForAdType:AppodealAdTypeSkippableVideo name:kAppodealAdColonyNetworkName];
        [Appodeal disableNetworkForAdType:AppodealAdTypeSkippableVideo name:kAppodealMopubNetworkName];
        [Appodeal disableNetworkForAdType:AppodealAdTypeSkippableVideo name:kAppodealAppLovinNetworkName];
        //[Appodeal disableNetworkForAdType:AppodealAdTypeSkippableVideo name:kAppodeal];
        
        iosDelegate = [[IOSAppodealDelegate alloc] init];
        [Appodeal confirmUsage:AppodealAdTypeSkippableVideo];
        [Appodeal initializeWithApiKey:NSStringFromStdString(key) types: AppodealAdTypeSkippableVideo];
        [Appodeal setInterstitialDelegate:iosDelegate];
        [Appodeal setVideoDelegate:iosDelegate];
        [Appodeal setRewardedVideoDelegate:iosDelegate];

        //setup with vk
     /*   VKAccessToken *vkToken = [VKSdk accessToken];
        if (vkToken && vkToken.userId) {
            [Appodeal setUserVkId:vkToken.userId];
        }
        
        //setup with fb
        FBSDKAccessToken *fbToken = [FBSDKAccessToken currentAccessToken];
        if (fbToken && fbToken.userID) {
            [Appodeal setUserFacebookId:fbToken.userID];
        } */
    }
    
    virtual void removeBanner() override {
        //fix: banner removed in destructor. Destructor may be called not on main thread
        dispatch_block_t block = ^{
            [Appodeal hideBanner];
        };
        if ([NSThread isMainThread]) {
            block();
        } else {
            dispatch_async(dispatch_get_main_queue(), block);
        }
    }
    
    virtual void presentBanner(float xPercent, float yPercent, float yAnchor) override {
        UIViewController *topVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        if (yPercent < 0.1f) {
            [Appodeal showAd:AppodealShowStyleBannerBottom rootViewController:topVC];
        } else if (yPercent > 0.9f) {
            [Appodeal showAd:AppodealShowStyleBannerCenter rootViewController:topVC];
        } else {
            [Appodeal showAd:AppodealShowStyleBannerTop rootViewController:topVC];
        }
    }
    
    virtual void presentInterstitial(std::function<void()> completion) override {
        iosDelegate.interstitialCompletion = completion;
        UIViewController *topVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        [Appodeal showAd:AppodealShowStyleInterstitial rootViewController:topVC];
    }
    
    virtual void presentVideoInterstitial(std::function<void()> completion) override {
        iosDelegate.interstitialCompletion = completion;
        UIViewController *topVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        [Appodeal showAd:AppodealShowStyleSkippableVideo rootViewController:topVC];
    }
    
    virtual void presentInterstitialStaticOrVideo(std::function<void()> completion) override {
        iosDelegate.interstitialCompletion = completion;
        UIViewController *topVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        [Appodeal showAd:AppodealShowStyleVideoOrInterstitial rootViewController:topVC];
    }
    
    virtual void presentRewardedVideo(std::function<void(bool success)> completion) override {
        iosDelegate.rewardedVideoCompletion = completion;
        UIViewController *topVC = [[[UIApplication sharedApplication] delegate] window].rootViewController;
        [Appodeal showAd:AppodealShowStyleRewardedVideo rootViewController:topVC];
    }
    
    virtual bool hasCachedRewardedVideo() const override {
        return [Appodeal isReadyForShowWithStyle:AppodealShowStyleRewardedVideo];
    }
};

std::shared_ptr<Advertise::AppodealAds> Advertise::AppodealAds::getInstance() {
    static auto instance = std::dynamic_pointer_cast<AppodealAds>(std::make_shared<IOSAppodeal>());
    return instance;
}
